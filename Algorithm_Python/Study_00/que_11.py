def solution(n):
    n = str(n)
    n = list(map(int, n))
    return sum(n)

print(solution(118372))
